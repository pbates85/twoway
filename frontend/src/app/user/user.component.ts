import {Component} from '@angular/core';
import {User} from "../models/user";
import {BehaviorSubject, Subscription} from "rxjs";
import {message} from "../models/message";
import {Router} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {UserService} from "../service/user.service";
import {MessageService} from "../service/message.service";
import {NotificationType} from "../enum/notification-type.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {resetParseTemplateAsSourceFileForTest} from "@angular/compiler-cli/src/ngtsc/typecheck/diagnostics";



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  private titleSubject = new BehaviorSubject<string>('Beta testing is free to use!');
  public titleAction$ = this.titleSubject.asObservable();
  public users!: User[];
  public user!: User;
  public refreshing!: boolean;
  public Messages: message[] = [];
  public yarnDto!: message;
  public sender!: string;
  public recipient!: string;
  public yarnPostDto!: message;
  public yarn!: string;
  public reversedMessages: message[] = [];
  private subscriptions: Subscription[] = [];
  public usernamePage = document.querySelector('#username-page');
  public chatPage = document.querySelector('#chat-page');
  public usernameForm = document.querySelector('#usernameForm');
  public messageForm = document.querySelector('#messageForm');
  public messageInput = document.querySelector('#message');
  public messageArea = document.querySelector('#messageArea');
  public connectingElement = document.querySelector('.connecting');
  username: string = '';

  constructor(private router: Router, private authenticationService: AuthenticationService,
              private userService: UserService, private notificationService: NotificationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    // call functions before the page loads
    this.user = this.authenticationService.getUserFromLocalCache();
    this.Messages = [];
  }

  public getUsers(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.userService.getUsers().subscribe(
        (response: User[]) => {
          const activeUsers = response.filter(user => user.active);
          this.userService.addUsersToLocalCache(activeUsers);
          this.users = activeUsers;
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `${activeUsers.length} active user(s) loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  private sendNotification(notificationType: NotificationType, message: string): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(notificationType, 'An error occurred. Please try again.');
    }
  }


  // Function to be called when the user scrolls down
  public onScroll(): void {
    // Check if the user has reached the bottom of the container
    const container = document.querySelector('#scroll-div'); // Use the hash symbol for ID selector
    // @ts-ignore
    if (container.scrollHeight - container.scrollTop === container.clientHeight) {

    }
  }

  // connect
  connect(): void {
    console.log("Connections")
    this.messageService.connect();

  }

  // on connected

  // error

  // send message

  // on message received




  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


}
