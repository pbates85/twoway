import { Injectable } from '@angular/core';
import { Client, Message, Stomp, Frame } from '@stomp/stompjs';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() {
    const config: SocketIoConfig = { url: 'http://localhost:8080', options: {} };
  }

  connect() {
  };

  disconnect() {
  }

  send(message: string) {
  }

  onMessageReceived(message: Message) {
    console.log("Message Recieved from Server :: " + message);
  }
}
