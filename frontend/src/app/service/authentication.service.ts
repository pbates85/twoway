import { Injectable } from '@angular/core';
import {environment } from '../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {User} from "../models/user";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private token!: string;
  private loggedInUsername!: string;
  public host = environment.apiUrl;
  // https://www.npmjs.com/package/@auth0/angular-jwt
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>
    (`${this.host}/user/login`, user, { observe: 'response' }); //route/body/metadata in header to pull the jwt token return the user and the response
  }

  public register(user: User): Observable<User> {
    return this.http.post<User>(`${this.host}/user/register`, user); // return just the user
  }

  // return void clear
  public logOut(): void {
    this.token = null!;
    this.loggedInUsername = null!;
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    localStorage.removeItem('users');
  }
  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public addUserToLocalCache(user: User): void {
    // local storage cannot take an object only strings
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUserFromLocalCache(): User {
    return JSON.parse(localStorage.getItem('user')!);
  }

  public loadToken(): void {
    this.token = localStorage.getItem('token')!;
  }

  public getToken(): string {
    return this.token;
  }

  //pre-check conditional for later routing
  public isUserLoggedIn(): boolean {
    this.loadToken();
    if (this.token != null && this.token !== ''){
      // check the subject for a username 'subject = sub'
      if (this.jwtHelper.decodeToken(this.token).sub != null || '') {
        // token expired?
        if (!this.jwtHelper.isTokenExpired(this.token)) {
          // logged in username
          this.loggedInUsername = this.jwtHelper.decodeToken(this.token).sub;
          return true;
        }
      }
    }
    this.logOut();
    return false;
  }
}
