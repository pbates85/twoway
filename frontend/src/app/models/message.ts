
export interface message {
  sender: string;
  recipient: string;
  content: string;
}
