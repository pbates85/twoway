//package com.trident.twoway.config;
//
//import no.finn.unleash.DefaultUnleash;
//import no.finn.unleash.Unleash;
//import no.finn.unleash.util.UnleashConfig;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class UnleashConfiguration {
//
//    public UnleashConfig unleashConfig() {
//        return UnleashConfig.builder()
//                .appName("staging")
//                .instanceId("hdC4K249PH--81DKqKuF")
//                .unleashAPI("https://gitlab.phillipbates.com/api/v4/feature_flags/unleash/1")
//                .environment("staging")
//                .build();
//    }
//
//    public Unleash unleash = new DefaultUnleash(unleashConfig());
//
//
//    @Bean
//    public void check(){
//        if(unleash.isEnabled("test-flag")) {
//            System.out.println("found feature");
//        } else {
//            System.out.println("Try again");
//        }
//    }
//}
