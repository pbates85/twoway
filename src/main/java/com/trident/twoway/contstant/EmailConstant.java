package com.trident.twoway.contstant;

import java.util.Map;


public class EmailConstant {
    static Map<String, String> env = System.getenv();

    public static final String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtp";
    public static final String USERNAME = "calypso_trident_solutions_us@outlook.com";

    public static final String PASSWORD = env.get("EMAIL_PW");
    public static final String FROM_EMAIL = "calypso_trident_solutions_us@outlook.com";
    public static final String CC_EMAIL = "";
    public static final String EMAIL_SUBJECT = "Calypso App - New Password";
    public static final String OUTLOOK_SMTP_SERVER = "smtp-mail.outlook.com";
    public static final String SMTP_HOST = "mail.smtp.host";
    public static final String SMTP_AUTH = "mail.smtp.auth";
    public static final int DEFAULT_PORT = 587;
    public static final String SMTP_PORT = "mail.smtp.port";
    public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
}

