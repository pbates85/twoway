package com.trident.twoway.contstant;

public class FileConstant {
    public static final String USER_IMAGE_PATH = "/api/user/image/";

    public static final String YACHT_IMAGE_PATH = "/api/yacht/image/";
    public static final String JPG_EXTENSION = "jpg";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/calypso/user/";

    public static final String YACHT_FOLDER = System.getProperty("user.home") + "/calypso/yacht/";
    public static final String DIRECTORY_CREATED = "Created directory for: ";
    public static final String DEFAULT_USER_IMAGE_PATH = "/api/user/image/profile/";

    public static final String DEFAULT_YACHT_IMAGE_PATH = "/api/yacht/image/profile/sail-silhouette.jpg";
    public static final String FILE_SAVED_IN_FILE_SYSTEM = "Saved file in file system by name: ";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String NOT_AN_IMAGE_FILE = " is not an image file. Please upload an image file";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";
}

