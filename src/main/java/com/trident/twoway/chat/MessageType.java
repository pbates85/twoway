package com.trident.twoway.chat;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}
